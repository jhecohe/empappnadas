from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def index(request):
   # return HttpResponse("Cooking with love to make yummy apps")
   return render(request, 'index.html')

